# cpp-ncurses-skeleton

A very basic skeleton application with a vi-like TUI interface.

Was looking to build a simple TUI application and I was surprised that
something like this didn't already exist. So had to build one myself. Skeleton
is meant for synchronous applications, however most of the implementation is
thread-safe to my knowledge.

Tested extensively on GNU/Linux but should work on other platforms as well.

Integrates with GNU's readline library, which is under GPLv3. If this is a
concern, you might want to re-implement `ui/cmdline_window.cpp` with `editline`
or `linenoise`.

This is one of my first serious c++ projects so send code review/PR if
necessary.

# Dependencies
- `C++11`
- GNU Readline
