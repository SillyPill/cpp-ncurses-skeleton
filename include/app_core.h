#ifndef APP_ACTIONS_H
#define APP_CORE_H

#include "app.h"

namespace AppCore{

enum Action
{
    QUIT,
    PROMPT_FOR_COMMAND,
    PROMPT_FOR_SEARCH_REGEX,
    RESIZE_UI
};

void app_do(Action action);

void show_msg( const std::string& );
void show_error( const std::string& );

}

#endif /* APP_ACTIONS_H */
