#ifndef CMDLINE_HANDLER_H
#define CMDLINE_HANDLER_H

#include <vector>
#include <string>
#include <map>
#include <functional>

class CmdlineHandler
{
private:
    std::vector<std::string> tokenize( std::string );
    static std::map
    < std::string
    , std::function<void( std::vector<std::string> )>
    >
    commands;

public:
    CmdlineHandler();
    explicit CmdlineHandler(std::string);
    //virtual ~CmdlineHandler();
};

#endif /* CMDLINE_HANDLER_H */
