#ifndef TITLE_WINDOW_H
#define TITLE_WINDOW_H

#include "window.h"
#include <string>

class TitleWindow : public NcWindow
{
private:
    static const std::string title;
    void init();

public:

    TitleWindow(unsigned int, unsigned int,
                unsigned int, unsigned int, unsigned int);
    /* virtual ~StatuslineWindow(); */
};

#endif /* TITLE_WINDOW_H */
