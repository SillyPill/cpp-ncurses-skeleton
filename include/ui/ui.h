#ifndef UI_H 
#define UI_H

#include "ui/main_window.h"
#include "ui/statusline_window.h"
#include "ui/command_window.h"
#include "window.h"
#include <ncurses.h>

class UI {
	private:
		int maxy, maxx;
		void curses_init();
		void windows_init();
		void delete_windows();
		void init_colors();

	public:
		NcWindow* title;
		StatuslineWindow* status;
		MainWindow* main_win;
        CommandWindow* cmd_win;

		UI();
		virtual ~UI();
		void resize();
        int get_input();
};

#endif /* UI_H */
