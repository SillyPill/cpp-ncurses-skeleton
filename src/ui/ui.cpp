#include "ui/ui.h"
#include "app.h"
#include "ui/colors.h"
#include "ui/command_window.h"
#include "ui/main_window.h"
#include "ui/statusline_window.h"
#include "ui/title_window.h"
#include "ui/window.h"
#include <iostream>
#include <ncurses.h>
#include <string>

UI::UI()
{
    curses_init();
    refresh();
    windows_init();
}

UI::~UI()
{
    delete_windows();
    clear();
    endwin();
}

void UI::curses_init()
{
    setlocale(LC_ALL, "");
    initscr();
    noecho(); // disable echo
    //cbreak();
    raw();

    curs_set(0); // hide cursor
    nonl();

    if (has_colors()) {
        start_color();
        use_default_colors();
        Colors::init_color_pairs();
    }
}

void UI::windows_init()
{

    refresh();

    getmaxyx(stdscr, maxy, maxx);

    //
    // Different windows for the application to be defined here
    //
    status = new StatuslineWindow(
        "New status",
        1, maxx, maxy - 2, 0,                             // Window Properties
        Colors::get_color_id(A_COLOR_BLACK, A_COLOR_BLUE) // Colors
    );

    title = new TitleWindow(
        1, maxx, 0, 0,
        Colors::get_color_id(A_COLOR_BLACK, A_COLOR_WHITE));

    unsigned int main_window_height = maxy - 3;

    main_win = new MainWindow(
        main_window_height, maxx, 1, 0,
        Colors::get_color_id(A_COLOR_WHITE));

    cmd_win = new CommandWindow(
        1, maxx, maxy - 1, 0,
        Colors::get_color_id(A_COLOR_GREEN));
}

void UI::delete_windows()
{
    delete title;
    delete status;
    delete main_win;
    endwin();
    refresh();
}

void UI::resize()
{
    //
    // clear everything!
    //
    delete_windows();

    //
    // initialize everything
    //
    windows_init();

    return;
}

int UI::get_input()
{
    return getch();
}
