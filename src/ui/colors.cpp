#include <iostream>
#include <ncurses.h>
#include "ui/colors.h"

void Colors::init_color_pairs( void )
{
    for (int fg = 0; fg < 16; fg++)
    {
        for (int bg = 0; bg < 16; bg++)
        {
            int color_id = fg<<4 | bg;
            if ( fg != bg )
            {
                init_pair( color_id, fg, bg  );
            }
            else
            {
                init_pair( color_id, fg, -1 );
            }
        }
    }
}

unsigned int Colors::get_color_id( const ColorIds& fg, const ColorIds& bg )
{
    int i_fg = (int) fg;     // ik.. enum is cast implicitly... but whatever
    int i_bg = (int) bg;
    int pair_id = i_fg << 4;
    pair_id += i_bg;
    return pair_id;
}

unsigned int Colors::get_color_id( const ColorIds& fg )
{
    int i_fg = (int) fg;
    int pair_id = i_fg * 16;
    pair_id += i_fg;
    return pair_id;
}

/* ----------REFERENCE---------- */
/* COLOR_BLACK   0 */
/* COLOR_RED     1 */
/* COLOR_GREEN   2 */
/* COLOR_YELLOW  3 */
/* COLOR_BLUE    4 */
/* COLOR_MAGENTA 5 */
/* COLOR_CYAN    6 */
/* COLOR_WHITE   7 */
