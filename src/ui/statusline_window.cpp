#include "ui/statusline_window.h"
#include <ncurses.h>
#include <stdexcept>
#include <string>

std::string StatuslineWindow::status = "";

StatuslineWindow::StatuslineWindow(const std::string& status_string,
    unsigned int height,
    unsigned int width,
    unsigned int start_y,
    unsigned int start_x,
    unsigned int colors)
    : NcWindow(height, width, start_y, start_x, colors)
{
    set_status(status_string);
}

void StatuslineWindow::set_status(std::string status_string)
{
    StatuslineWindow::status = status_string;
    show();
}

void StatuslineWindow::show()
{
    werase(win);
    wprintw(win, "%s", status.c_str());
    wrefresh(win);
}

/* StatuslineWindow::print_error */
