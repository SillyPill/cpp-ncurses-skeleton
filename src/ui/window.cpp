#include "ui/window.h"
#include <ncurses.h>
#include <stdexcept>
#include <string>

NcWindow::NcWindow(unsigned int height,
                   unsigned int width,
                   unsigned int start_y,
                   unsigned int start_x,
                   unsigned int colors)
    : height(height)
    , width(width)
    , start_y(start_y)
    , start_x(start_x)
{
    if (height < 1 || width < 1)
    {
        throw std::invalid_argument("Window dimentions can't be 0");
    }

    win = newwin(height, width, start_y, start_x);

    wbkgd(win, COLOR_PAIR(colors));
    wrefresh(win);
}

void NcWindow::say(const std::string& s)
{
    mvwprintw(win, 0, 0, "%s", s.c_str());
    wrefresh(win);
}

void NcWindow::erase( void )
{
    werase(win);
    wmove(win,0,0);
    wrefresh(win);
}
