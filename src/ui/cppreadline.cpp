#include "ui/cppreadline.h"

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <sstream>
#include <unordered_set>

#include <cstdlib>
#include <cstring>
#include <readline/history.h>
#include <readline/readline.h>

namespace CppReadline {
namespace {

    Console* currentConsole = nullptr;
    HISTORY_STATE* emptyHistory = history_get_history_state();

} /* namespace  */

struct Console::Impl {
    using RegisteredCommands = std::unordered_set<std::string>;

    ::std::string greeting_;
    RegisteredCommands commands_;
    HISTORY_STATE* history_ = nullptr;

    Impl(::std::string const& greeting)
        : greeting_(greeting)
        , commands_()
    {
    }
    ~Impl()
    {
        free(history_);
    }

    Impl(Impl const&) = delete;
    Impl(Impl&&) = delete;
    Impl& operator=(Impl const&) = delete;
    Impl& operator=(Impl&&) = delete;
};

int abort_prompt(int, int)
{
    throw Readline_Aborted();
    rl_done = 1;
    return 0;
}
// Quitting behaviour is hardcoded in readLine()
Console::Console(std::string const& greeting)
    : pimpl_ { new Impl { greeting } }
{
    // Init readline basics
    init_readline();
}

void Console::init_readline(void)
{
    // initialize readline (needed, otherwise we get segmentation
    // fault on SIGWINCH). also, initialize first as doing this
    // later erases keys bound with rl_bind_key for some users.
    /* rl_initialize(); */

    // if ctrl-c is pressed, throw exception
    rl_bind_key('\3', abort_prompt);

    // do not catch signals
    //rl_catch_signals = 0;
    rl_attempted_completion_function = &Console::getCommandCompletions;
}

Console::Console(std::string const& greeting,
    const std::unordered_set<std::string>& command)
    : pimpl_ { new Impl { greeting } }
{
    // Init readline basics
    init_readline();
    for (auto itr = command.begin(); itr != command.end(); ++itr) {
        pimpl_->commands_.insert(*itr);
    }
}

Console::~Console() = default;

void Console::registerCommand(const std::string& s)
{
    pimpl_->commands_.insert(s);
}

std::vector<std::string> Console::getRegisteredCommands() const
{
    std::vector<std::string> allCommands;
    for (auto& command : pimpl_->commands_)
        allCommands.push_back(command);

    return allCommands;
}

void Console::saveState()
{
    free(pimpl_->history_);
    pimpl_->history_ = history_get_history_state();
}

void Console::reserveConsole()
{
    if (currentConsole == this)
        return;

    // Save state of other Console
    if (currentConsole)
        currentConsole->saveState();

    // Else we swap state
    if (!pimpl_->history_)
        history_set_history_state(emptyHistory);
    else
        history_set_history_state(pimpl_->history_);

    // Tell others we are using the console
    currentConsole = this;
}

void Console::setGreeting(const std::string& greeting)
{
    pimpl_->greeting_ = greeting;
}

std::string Console::getGreeting() const
{
    return pimpl_->greeting_;
}

std::string Console::readLine()
{
    reserveConsole();

    char* buffer = readline(pimpl_->greeting_.c_str());

    std::string line;

    // return empty string if the input is EOF
    if (buffer) {
        line = std::string(buffer);
        // Add to history only if line was not empty
        if (buffer[0] != '\0')
            add_history(buffer);

    } else {
        throw Readline_EOF();
    }

    free(buffer);

    return line;
}

char** Console::getCommandCompletions(const char* text, int start, int /*end*/)
{
    if (start == 0)
        return rl_completion_matches(text, &Console::commandIterator);
    else
        return nullptr;
}

char* Console::commandIterator(const char* text, int state)
{
    static Impl::RegisteredCommands::iterator it;
    if (!currentConsole)
        return nullptr;
    auto& commands = currentConsole->pimpl_->commands_;

    if (state == 0)
        it = begin(commands);

    while (it != end(commands)) {
        auto& command = *it;
        ++it;
        if (command.find(text) != std::string::npos) {
            return strdup(command.c_str());
        }
    }
    return nullptr;
}
}
