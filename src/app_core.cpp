#include "app.h"
#include "app_core.h"
#include "ui/cppreadline.h"
#include "cmdline_handler.h"
#include "ui/colors.h"
#include <string>


void AppCore::app_do(Action action)
{

    switch (action)
    {
        case QUIT:
            {
                app->quit();
                break;
            }
        case PROMPT_FOR_COMMAND:
            {
                std::string command;
                command = app->ui->cmd_win->prompt(":");
                app->ui->main_win->erase();
                app->ui->main_win->say(command);
                CmdlineHandler temp(command);
                break;
            }
        case RESIZE_UI:
            {
                app->ui->resize();
                break;
            }
        case PROMPT_FOR_SEARCH_REGEX:
            {
                std::string command;
                command = app->ui->cmd_win->prompt("/");
                app->ui->main_win->erase();
                app->ui->main_win->say(command);
                break;
            }
        default:
            ;

    }
}

void AppCore::show_msg( const std::string& msg )
{
    app->ui->cmd_win->say(msg);
}

void AppCore::show_error( const std::string& err )
{
    app->ui->cmd_win->say(err,
            Colors::get_color_id(A_COLOR_B_WHITE, A_COLOR_B_RED));
}

