#include <ncurses.h>
#include <unordered_map>
#include <memory>

#include "app_core.h"
#include "input_handler.h"

int InputHandler::repeat_count = 0;

std::shared_ptr<KeyMapNode> InputHandler::keymap_root_node;
std::shared_ptr<KeyMapNode> InputHandler::curr_node;

InputHandler::InputHandler()
{
    keymap_root_node = std::make_shared<KeyMapNode>();
    curr_node = keymap_root_node;
}

InputHandler::~InputHandler()
{}

void InputHandler::handle( int c )
{
    if (c == KEY_RESIZE)
    {
        AppCore::app_do(AppCore::RESIZE_UI);
    }
    else if ( c > 47 && c < 58 )
    {
        repeat_count *= 10;
        repeat_count += c - 48; // convert ASCII value to integer
    }
    else if ( c == 27 ) // on ESC key, reset
    {
        curr_node = keymap_root_node;
    }
    else
    {
        //
        //if 'c' not exists in current subtree, reset tree
        //
        if (curr_node->branches.find((char)c) == curr_node->branches.end())
        {
            curr_node = keymap_root_node;
            repeat_count = 0;
        }
        else
        {
            curr_node = curr_node->branches[ (char) c];

            //
            //if function found, call it and reset
            //
            if (curr_node->func != nullptr)
            {
                curr_node->func(repeat_count == 0 ? 1 : repeat_count);
                curr_node = keymap_root_node;
                repeat_count = 0;
            }
        }
    }
}


void InputHandler::register_map( std::string str, std::function<void(int)> registered_func )
{
    std::shared_ptr<KeyMapNode> temp = keymap_root_node;
    for (char& c : str)
    {
        // If character 'c' not found in subtrees
        if ((temp->branches).count(c) == 0 )
        {
            std::shared_ptr<KeyMapNode> new_node_ptr =
                std::make_shared<KeyMapNode>();
            temp->branches.insert({ c, new_node_ptr });
        }
        temp = temp->branches[c];
    }
    temp->func = registered_func;
}
