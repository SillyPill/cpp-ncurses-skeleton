#include "app.h"
#include "input_handler.h"
#include "key_bindings.h"
#include "ui/ui.h"
#include <iostream>
#include <string>

App* app;

const std::string App::app_name = "Curses Example";
const std::string App::app_version = "0.0.1";

UI* App::ui;

bool App::stop_app;

int App::run()
{
    InputHandler handler;
    KeyBindings::register_defaults(handler);
    init_ui();

    do {
        handler.handle(ui->get_input());
    } while (!stop_app);

    return 0;
}

App::App()
{
    init_ui();
}

App::~App()
{
    delete ui;
}

void App::init_ui()
{
    ui = new UI();
}

const std::string App::getAppName()
{
    return app_name;
}

const std::string App::getAppVersion()
{
    return app_version;
}

void App::quit()
{
    stop_app = true;
}
